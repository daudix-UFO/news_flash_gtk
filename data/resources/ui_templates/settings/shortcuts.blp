using Gtk 4.0;
using Adw 1;

template $SettingsShortcutsPage : Adw.PreferencesPage {
  icon-name: "input-keyboard-symbolic";
  title: "Keybindings";

  Adw.PreferencesGroup {
    title: "Article List";

    Adw.ActionRow next_article_row {
      title: _("Next Article");
      name: "next_article_row";
      activatable: true;

      Label next_article_label {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;

        styles [
          "dim-label",
        ]
      }
    }

    Adw.ActionRow previous_article_row {
      title: _("Previous Article");
      name: "previous_article_row";
      activatable: true;

      Label previous_article_label {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;

        styles [
          "dim-label",
        ]
      }
    }

    Adw.ActionRow toggle_read_row {
      title: _("Toggle Read");
      name: "toggle_read_row";
      activatable: true;

      Label toggle_read_label {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;

        styles [
          "dim-label",
        ]
      }
    }

    Adw.ActionRow toggle_marked_row {
      title: _("Toggle Starred");
      name: "toggle_marked_row";
      activatable: true;

      Label toggle_marked_label {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;

        styles [
          "dim-label",
        ]
      }
    }

    Adw.ActionRow open_browser_row {
      title: _("Open URL");
      name: "open_browser_row";
      activatable: true;

      Label open_browser_label {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;

        styles [
          "dim-label",
        ]
      }
    }

    Adw.ActionRow copy_url_row {
      title: _("Copy URL");
      name: "copy_url_row";
      activatable: true;

      Label copy_url_label {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;

        styles [
          "dim-label",
        ]
      }
    }
  }

  Adw.PreferencesGroup {
    title: "Feed List";

    Adw.ActionRow next_item_row {
      title: _("Next Item");
      name: "next_item_row";
      activatable: true;

      Label next_item_label {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;

        styles [
          "dim-label",
        ]
      }
    }

    Adw.ActionRow previous_item_row {
      title: _("Previous Item");
      name: "previous_item_row";
      activatable: true;

      Label previous_item_label {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;

        styles [
          "dim-label",
        ]
      }
    }

    Adw.ActionRow toggle_category_expanded_row {
      title: _("Expand / Collapse");
      name: "toggle_category_expanded_row";
      activatable: true;

      Label toggle_category_expanded_label {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;

        styles [
          "dim-label",
        ]
      }
    }

    Adw.ActionRow sidebar_set_read_row {
      title: _("Mark selected read");
      name: "sidebar_set_read_row";
      activatable: true;

      Label sidebar_set_read_label {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;

        styles [
          "dim-label",
        ]
      }
    }
  }

  Adw.PreferencesGroup {
    title: "General";

    Adw.ActionRow shortcuts_row {
      title: _("Shortcuts");
      name: "shortcuts_row";
      activatable: true;

      Label shortcuts_label {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;

        styles [
          "dim-label",
        ]
      }
    }

    Adw.ActionRow refresh_row {
      title: _("Refresh");
      name: "refresh_row";
      activatable: true;

      Label refresh_label {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;

        styles [
          "dim-label",
        ]
      }
    }

    Adw.ActionRow search_row {
      title: _("Search");
      name: "search_row";
      activatable: true;

      Label search_label {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;

        styles [
          "dim-label",
        ]
      }
    }

    Adw.ActionRow quit_row {
      title: _("Quit");
      name: "quit_row";
      activatable: true;

      Label quit_label {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;

        styles [
          "dim-label",
        ]
      }
    }

    Adw.ActionRow all_articles_row {
      title: _("All Articles");
      name: "all_articles_row";
      activatable: true;

      Label all_articles_label {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;

        styles [
          "dim-label",
        ]
      }
    }

    Adw.ActionRow only_unread_row {
      title: _("Only Unread");
      name: "only_unread_row";
      activatable: true;

      Label only_unread_label {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;

        styles [
          "dim-label",
        ]
      }
    }

    Adw.ActionRow only_starred_row {
      title: _("Only Starred");
      name: "only_starred_row";
      activatable: true;

      Label only_starred_label {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;

        styles [
          "dim-label",
        ]
      }
    }
  }

  Adw.PreferencesGroup {
    title: "Article View";

    Adw.ActionRow scroll_up_row {
      title: _("Scroll up");
      name: "scroll_up_row";
      activatable: true;

      Label scroll_up_label {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;

        styles [
          "dim-label",
        ]
      }
    }

    Adw.ActionRow scroll_down_row {
      title: _("Scroll down");
      name: "scroll_down_row";
      activatable: true;

      Label scroll_down_label {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;

        styles [
          "dim-label",
        ]
      }
    }

    Adw.ActionRow scrap_content_row {
      title: _("Scrape article content");
      name: "scrap_content_row";
      activatable: true;

      Label scrap_content_label {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;

        styles [
          "dim-label",
        ]
      }
    }

    Adw.ActionRow tag_row {
      title: _("Tag article");
      name: "tag_row";
      activatable: true;

      Label tag_label {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;

        styles [
          "dim-label",
        ]
      }
    }

    Adw.ActionRow fullscreen_row {
      title: _("Fullscreen article");
      name: "fullscreen_row";
      activatable: true;

      Label fullscreen_label {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;

        styles [
          "dim-label",
        ]
      }
    }
  }
}
