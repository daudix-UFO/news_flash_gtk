use serde::{Deserialize, Serialize};
use std::default::Default;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum ProxyProtocoll {
    All,
    Http,
    Https,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ProxyModel {
    pub protocoll: ProxyProtocoll,
    pub url: String,
    pub user: Option<String>,
    pub password: Option<String>,
}

const fn _default_true() -> bool {
    true
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct AdvancedSettings {
    pub proxy: Vec<ProxyModel>,
    pub accept_invalid_certs: bool,
    pub accept_invalid_hostnames: bool,
    #[serde(default)]
    pub inspect_article_view: bool,
    #[serde(default = "_default_true")]
    pub article_view_load_images: bool,
}

impl Default for AdvancedSettings {
    fn default() -> Self {
        AdvancedSettings {
            proxy: Vec::new(),
            accept_invalid_certs: false,
            accept_invalid_hostnames: false,
            inspect_article_view: false,
            article_view_load_images: true,
        }
    }
}
