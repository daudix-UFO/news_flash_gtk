mod service_row;

use self::service_row::ServiceRow;
use crate::app::App;
use glib::{clone, subclass};
use gtk4::{prelude::*, subclass::prelude::*};
use gtk4::{CompositeTemplate, ListBox};
use news_flash::models::{LoginData, LoginGUI, PluginID, PluginInfo};
use news_flash::NewsFlash;
use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/login/welcome.blp")]
    pub struct WelcomePage {
        pub services: Rc<RefCell<HashMap<i32, (PluginID, LoginGUI)>>>,

        #[template_child]
        pub list: TemplateChild<ListBox>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for WelcomePage {
        const NAME: &'static str = "WelcomePage";
        type ParentType = gtk4::Box;
        type Type = super::WelcomePage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for WelcomePage {
        fn constructed(&self) {
            self.obj().init();
        }
    }

    impl WidgetImpl for WelcomePage {}

    impl BoxImpl for WelcomePage {}
}

glib::wrapper! {
    pub struct WelcomePage(ObjectSubclass<imp::WelcomePage>)
        @extends gtk4::Widget, gtk4::Box;
}

impl Default for WelcomePage {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl WelcomePage {
    pub fn new() -> Self {
        Self::default()
    }

    fn init(&self) {
        self.populate();
        self.connect_signals();
    }

    fn populate(&self) {
        let services = NewsFlash::list_backends();
        let imp = self.imp();

        for (index, (id, plugin_info)) in services.into_iter().enumerate() {
            let row = ServiceRow::new();
            row.init(&plugin_info);
            imp.list.insert(&row, index as i32);
            let PluginInfo {
                id: _,
                name: _,
                icon: _,
                icon_symbolic: _,
                website: _,
                service_type: _,
                license_type: _,
                service_price: _,
                login_gui,
            } = plugin_info;
            imp.services.borrow_mut().insert(index as i32, (id, login_gui));
        }
    }

    fn connect_signals(&self) {
        let imp = self.imp();

        imp.list.connect_row_activated(
            clone!(@strong imp.services as services => @default-panic, move |_list, row| {
                if let Some((id, login_desc)) = services.borrow().get(&row.index()) {
                    match login_desc {
                        LoginGUI::OAuth(_) | LoginGUI::Direct(_) => {
                            App::default().main_window().show_login_page(id, None)
                        }
                        LoginGUI::None => {
                            App::default().login(LoginData::None(id.clone()));
                        }
                    };
                }
            }),
        );
    }
}
