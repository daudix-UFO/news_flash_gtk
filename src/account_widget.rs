use gtk4::{subclass::prelude::*, Box, CompositeTemplate, Image, Label, Widget};
use news_flash::models::VectorIcon;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/sidebar/account_widget.blp")]
    pub struct AccountWidget {
        #[template_child]
        pub logo: TemplateChild<Image>,
        #[template_child]
        pub user_label: TemplateChild<Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AccountWidget {
        const NAME: &'static str = "AccountWidget";
        type Type = super::AccountWidget;
        type ParentType = Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for AccountWidget {}

    impl WidgetImpl for AccountWidget {}

    impl BoxImpl for AccountWidget {}
}

glib::wrapper! {
    pub struct AccountWidget(ObjectSubclass<imp::AccountWidget>)
        @extends Widget, Box;
}

impl Default for AccountWidget {
    fn default() -> Self {
        glib::Object::new()
    }
}

impl AccountWidget {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn set_account(&self, vector_icon: Option<VectorIcon>, user_name: &str) {
        let imp = self.imp();

        imp.user_label.set_text(user_name);
        imp.logo.set_from_icon_name(Some("feed-service-generic"));

        if let Some(vector_icon) = vector_icon {
            let bytes = glib::Bytes::from_owned(vector_icon.data);
            let texture = gdk4::Texture::from_bytes(&bytes);
            imp.logo.set_from_paintable(texture.ok().as_ref());
        }
    }
}
