mod add_category_widget;
mod add_error_widget;
mod add_feed_widget;
mod add_tag_widget;
mod g_feed;
mod g_parsed_url;
mod parse_feed_widget;
mod select_feed_widget;

use self::add_category_widget::AddCategoryWidget;
use self::add_error_widget::AddErrorWidget;
use self::add_feed_widget::AddFeedWidget;
use self::add_tag_widget::AddTagWidget;
use self::g_feed::GFeed;
pub use self::g_parsed_url::GParsedUrl;
use self::parse_feed_widget::ParseFeedWidget;
use self::select_feed_widget::SelectFeedWidget;

use crate::app::App;
use glib::{clone, subclass};
use gtk4::{prelude::*, subclass::prelude::*, CompositeTemplate, ListBox, NamedAction, Shortcut, Widget, Window};
use libadwaita::{
    subclass::prelude::AdwWindowImpl, traits::ActionRowExt, ActionRow, NavigationView, Window as AdwWindow,
};
use news_flash::models::{Feed, PluginCapabilities, Url};
use news_flash::ParsedUrl;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/add_dialog/dialog.blp")]
    pub struct AddDialog {
        #[template_child]
        pub navigation_view: TemplateChild<NavigationView>,
        #[template_child]
        pub select_list_box: TemplateChild<ListBox>,
        #[template_child]
        pub feed_row: TemplateChild<ActionRow>,
        #[template_child]
        pub category_row: TemplateChild<ActionRow>,
        #[template_child]
        pub tag_row: TemplateChild<ActionRow>,
        #[template_child]
        pub close_shortcut: TemplateChild<Shortcut>,

        #[template_child]
        pub add_tag_widget: TemplateChild<AddTagWidget>,
        #[template_child]
        pub add_category_widget: TemplateChild<AddCategoryWidget>,
        #[template_child]
        pub parse_feed_widget: TemplateChild<ParseFeedWidget>,
        #[template_child]
        pub select_feed_widget: TemplateChild<SelectFeedWidget>,
        #[template_child]
        pub add_feed_widget: TemplateChild<AddFeedWidget>,
        #[template_child]
        pub error_widget: TemplateChild<AddErrorWidget>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AddDialog {
        const NAME: &'static str = "AddDialog";
        type ParentType = AdwWindow;
        type Type = super::AddDialog;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for AddDialog {}

    impl WidgetImpl for AddDialog {}

    impl WindowImpl for AddDialog {}

    impl AdwWindowImpl for AddDialog {}
}

glib::wrapper! {
    pub struct AddDialog(ObjectSubclass<imp::AddDialog>)
        @extends Widget, gtk4::Window, AdwWindow;
}

impl AddDialog {
    pub fn new_for_feed_url<W: IsA<Window> + GtkWindowExt>(parent: &W, feed_url: &Url) -> Self {
        let dialog = Self::new(parent);
        let imp = dialog.imp();
        imp.navigation_view.replace_with_tags(&["feed_parse_page"]);
        imp.parse_feed_widget.parse_feed_url(feed_url);
        dialog
    }

    pub fn new<W: IsA<Window> + GtkWindowExt>(parent: &W) -> Self {
        let dialog = glib::Object::new::<Self>();
        dialog.set_transient_for(Some(parent));

        let imp = dialog.imp();

        imp.parse_feed_widget.connect_local(
            "feed-parsed",
            false,
            clone!(@weak dialog => @default-panic, move |args| {
                let g_parsed_url = args[1].get::<GParsedUrl>()
                    .expect("The value needs to be of type `GParsedUrl`.");
                let parsed_url: ParsedUrl = g_parsed_url.into();

                let imp = dialog.imp();
                match parsed_url {
                    ParsedUrl::SingleFeed(feed) => {
                        imp.add_feed_widget.fill(*feed);
                        imp.navigation_view.push_by_tag("feed_add_page");
                    },
                    ParsedUrl::MultipleFeeds(feed_vec) => {
                        imp.select_feed_widget.fill(feed_vec);
                        imp.navigation_view.push_by_tag("feed_selection_page");
                    },
                }
                None
            }),
        );

        imp.parse_feed_widget.connect_local(
            "error",
            false,
            clone!(@weak dialog => @default-panic, move |args| {
                let error_msg = args[1].get::<String>()
                    .expect("The value needs to be of type `String`.");

                let imp = dialog.imp();
                imp.error_widget.set_error(&error_msg);
                imp.navigation_view.push_by_tag("error_page");
                None
            }),
        );

        imp.select_feed_widget.connect_local(
            "selected",
            false,
            clone!(@weak dialog => @default-panic, move |args| {
                let g_feed = args[1].get::<GFeed>()
                    .expect("The value needs to be of type `GFeed`.");
                let feed: Feed = g_feed.into();

                let imp = dialog.imp();
                imp.add_feed_widget.fill(feed);
                imp.navigation_view.push_by_tag("feed_add_page");
                None
            }),
        );

        imp.select_feed_widget.connect_local(
            "error",
            false,
            clone!(@weak dialog => @default-panic, move |args| {
                let error_msg = args[1].get::<String>()
                    .expect("The value needs to be of type `String`.");

                let imp = dialog.imp();
                imp.error_widget.set_error(&error_msg);
                imp.navigation_view.push_by_tag("error_page");
                None
            }),
        );

        imp.error_widget.connect_local(
            "try-again",
            false,
            clone!(@weak dialog => @default-panic, move |_| {
                let imp = dialog.imp();
                imp.parse_feed_widget.reset();
                imp.navigation_view.pop_to_tag("feed_parse_page");
                None
            }),
        );

        imp.close_shortcut.set_action(Some(NamedAction::new("window.close")));
        imp.add_tag_widget.connect_local(
            "tag-added",
            false,
            clone!(@weak dialog => @default-panic, move |_| {
                dialog.close();
                None
            }),
        );

        imp.add_category_widget.connect_local(
            "category-added",
            false,
            clone!(@weak dialog => @default-panic, move |_| {
                dialog.close();
                None
            }),
        );

        imp.add_feed_widget.connect_local(
            "feed-added",
            false,
            clone!(@weak dialog => @default-panic, move |_| {
                dialog.close();
                None
            }),
        );

        let features = App::default().features();
        imp.feed_row
            .set_sensitive(features.contains(PluginCapabilities::ADD_REMOVE_FEEDS));
        imp.category_row
            .set_sensitive(features.contains(PluginCapabilities::MODIFY_CATEGORIES));
        imp.tag_row
            .set_sensitive(features.contains(PluginCapabilities::SUPPORT_TAGS));

        imp.feed_row
            .connect_activated(clone!(@weak dialog => @default-panic, move |_row| {
                dialog.imp().navigation_view.push_by_tag("feed_parse_page");
            }));
        imp.category_row
            .connect_activated(clone!(@weak dialog => @default-panic, move |_row| {
                dialog.imp().navigation_view.push_by_tag("category_page");
            }));
        imp.tag_row
            .connect_activated(clone!(@weak dialog => @default-panic, move |_row| {
                dialog.imp().navigation_view.push_by_tag("tag_page");
            }));

        imp.navigation_view.replace_with_tags(&["start"]);

        dialog
    }
}
