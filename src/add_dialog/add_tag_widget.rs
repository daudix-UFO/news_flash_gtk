use crate::app::App;
use crate::color::ColorRGBA;
use glib::{clone, subclass::*};
use gtk4::{prelude::*, subclass::prelude::*, Box, Button, CompositeTemplate, Widget};
use libadwaita::{traits::EntryRowExt, EntryRow};

mod imp {
    use super::*;
    use glib::subclass;
    use once_cell::sync::Lazy;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/add_dialog/tag.blp")]
    pub struct AddTagWidget {
        #[template_child]
        pub add_tag_button: TemplateChild<Button>,
        #[template_child]
        pub tag_entry: TemplateChild<EntryRow>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AddTagWidget {
        const NAME: &'static str = "AddTagWidget";
        type ParentType = Box;
        type Type = super::AddTagWidget;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for AddTagWidget {
        fn signals() -> &'static [Signal] {
            static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| vec![Signal::builder("tag-added").build()]);
            SIGNALS.as_ref()
        }

        fn constructed(&self) {
            self.obj().init();
        }
    }

    impl WidgetImpl for AddTagWidget {}

    impl BoxImpl for AddTagWidget {}
}

glib::wrapper! {
    pub struct AddTagWidget(ObjectSubclass<imp::AddTagWidget>)
        @extends Widget, Box;
}

impl Default for AddTagWidget {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl AddTagWidget {
    pub fn new() -> Self {
        Self::default()
    }

    fn init(&self) {
        let imp = self.imp();

        let tag_add_button = imp.add_tag_button.get();
        let tag_entry = imp.tag_entry.get();

        // make parse button sensitive if entry contains text and vice versa
        imp.tag_entry
            .connect_changed(clone!(@weak tag_add_button => @default-panic, move |entry| {
                tag_add_button.set_sensitive(!entry.text().as_str().is_empty());
            }));

        // hit enter in entry to add tag
        imp.tag_entry
            .connect_entry_activated(clone!(@weak tag_add_button => @default-panic, move |_entry| {
                if tag_add_button.get_sensitive() {
                    tag_add_button.emit_clicked();
                }
            }));

        imp.add_tag_button.connect_clicked(clone!(
            @weak self as widget,
            @weak tag_entry => @default-panic, move |_button|
        {
            let rgba = ColorRGBA::new(25, 220, 114, 1);
            let color = rgba.as_string_no_alpha();
            if !tag_entry.text().as_str().is_empty() {
                App::default().add_tag(color, tag_entry.text().as_str().into(), None);
                widget.emit_by_name::<()>("tag-added", &[]);
            }
        }));
    }

    pub fn reset(&self) {
        self.imp().tag_entry.set_text("");
    }
}
