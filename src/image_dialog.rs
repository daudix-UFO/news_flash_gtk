use crate::app::App;
use base64::engine::general_purpose::STANDARD as base64_std;
use base64::Engine;
use bytesize::ByteSize;
use gdk4::Texture;
use gio::{SimpleAction, SimpleActionGroup};
use glib::{clone, subclass};
use gtk4::{
    prelude::*, subclass::prelude::*, CompositeTemplate, ContentFit, GestureClick, Label, NamedAction, Picture,
    Shortcut, Stack, Widget, Window,
};
use libadwaita::{subclass::prelude::AdwWindowImpl, Window as AdwWindow};
use news_flash::{
    error::NewsFlashError,
    models::{ArticleID, Url},
    Progress,
};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/media/image_dialog.blp")]
    pub struct ImageDialog {
        #[template_child]
        pub picture: TemplateChild<Picture>,
        #[template_child]
        pub picture_stack: TemplateChild<Stack>,
        #[template_child]
        pub progress_label: TemplateChild<Label>,
        #[template_child]
        pub close_shortcut: TemplateChild<Shortcut>,
        #[template_child]
        pub fullscreen_click: TemplateChild<GestureClick>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ImageDialog {
        const NAME: &'static str = "ImageDialog";
        type ParentType = AdwWindow;
        type Type = super::ImageDialog;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ImageDialog {}

    impl WidgetImpl for ImageDialog {}

    impl WindowImpl for ImageDialog {}

    impl AdwWindowImpl for ImageDialog {}
}

glib::wrapper! {
    pub struct ImageDialog(ObjectSubclass<imp::ImageDialog>)
        @extends Widget, gtk4::Window, AdwWindow;
}

impl ImageDialog {
    pub fn new_url(article_id: &ArticleID, url: &Url) -> Self {
        let dialog = glib::Object::new::<Self>();
        dialog.set_transient_for(Some(&App::default().main_window()));
        dialog.setup_actions();
        dialog.setup_fullscreen();

        let (sender, mut receiver) = tokio::sync::mpsc::channel::<Progress>(2);

        let imp = dialog.imp();
        let progress_label = imp.progress_label.clone();
        glib::MainContext::default().spawn_local(async move {
            while let Some(progress) = receiver.recv().await {
                let downloaded = ByteSize(progress.downloaded as u64);
                let total = ByteSize(progress.total_size as u64);
                let progress_text = format!("{downloaded} of {total}");
                if !progress_label.is_visible() {
                    progress_label.set_visible(true);
                }
                progress_label.set_text(&progress_text);
                log::debug!("{}", progress_text);
            }
        });

        imp.fullscreen_click
            .connect_released(clone!(@weak dialog => @default-panic, move |_event, times, _x, _y| {
                if times != 2 {
                    return;
                }

                if dialog.is_fullscreen() {
                    dialog.unfullscreen();
                } else {
                    dialog.set_modal(false);
                }
            }));

        let url = url.clone();
        let article_id = article_id.clone();
        App::default().execute_with_callback(
            |news_flash, client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash
                        .get_image(&article_id, url.as_str(), &client, Some(sender))
                        .await
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            clone!(@weak dialog => @default-panic, move |_app, res| {
                match res {
                    Ok(res) => {
                        dialog.imp().picture_stack.set_visible_child_name("picture");

                        let bytes  = glib::Bytes::from_owned(res);
                        let texture = Texture::from_bytes(&bytes).unwrap();
                        dialog.imp().picture.set_paintable(Some(&texture));

                        let (width, height) = dialog.calculate_size(texture.width(), texture.height());
                        dialog.set_width_request(width);
                        dialog.set_height_request(height);
                    }
                    Err(error) => {
                        dialog.close();
                        App::default().in_app_error("Failed to downlaod image", error);
                    }
                }
            }),
        );

        dialog
    }

    pub fn new_base64_data(base64_data: &str) -> Option<Self> {
        if let Some(decoded_data) = base64_data
            .find(',')
            .and_then(|start| base64_std.decode(&base64_data[start + 1..]).ok())
        {
            let dialog = glib::Object::new::<Self>();
            dialog.set_transient_for(Some(&App::default().main_window()));
            dialog.setup_actions();

            dialog.imp().picture_stack.set_visible_child_name("picture");

            let bytes = glib::Bytes::from_owned(decoded_data);
            let texture = Texture::from_bytes(&bytes).unwrap();
            dialog.imp().picture.set_paintable(Some(&texture));

            let (width, height) = dialog.calculate_size(texture.width(), texture.height());
            dialog.set_width_request(width);
            dialog.set_height_request(height);
            Some(dialog)
        } else {
            App::default().in_app_notifiaction("Could not decode image");
            None
        }
    }

    fn setup_actions(&self) {
        self.imp()
            .close_shortcut
            .set_action(Some(NamedAction::new("window.escape")));

        let escape_action = SimpleAction::new("escape", None);
        escape_action.connect_activate(
            clone!(@weak self as dialog => @default-panic, move |_action, _parameter| {
                if dialog.is_fullscreen() {
                    dialog.unfullscreen();
                } else {
                    dialog.close();
                }
            }),
        );

        let fullscreen_action = SimpleAction::new("fullscreen", None);
        fullscreen_action.connect_activate(
            clone!(@weak self as dialog => @default-panic, move |_action, _parameter| {
                if dialog.is_fullscreen() {
                    dialog.unfullscreen();
                } else {
                    dialog.set_modal(false);
                }
            }),
        );

        let action_group = SimpleActionGroup::new();
        action_group.add_action(&escape_action);
        action_group.add_action(&fullscreen_action);
        self.insert_action_group("window", Some(&action_group));
    }

    fn setup_fullscreen(&self) {
        self.connect_modal_notify(|dialog| {
            if !dialog.is_modal() {
                dialog.set_transient_for(None::<&Window>);
                dialog.fullscreen();
                dialog.imp().picture.set_content_fit(ContentFit::Contain);
            }
        });
        self.connect_fullscreened_notify(|dialog| {
            if !dialog.is_fullscreen() {
                dialog.set_transient_for(Some(&App::default().main_window()));
                dialog.set_modal(true);
                dialog.imp().picture.set_content_fit(ContentFit::Cover);
            }
        });
    }

    fn calculate_size(&self, image_width: i32, image_height: i32) -> (i32, i32) {
        let main_window = App::default().main_window();
        let parent_width = main_window.width();
        let parent_height = main_window.height();

        if image_width < parent_width && image_height < parent_height {
            return (image_width, image_height);
        }

        let mut scale = parent_width as f64 / image_width as f64;
        if image_height as f64 * scale > parent_height as f64 {
            scale = parent_height as f64 / image_height as f64;
        }
        let scale = scale * 0.95;

        let width = image_width as f64 * scale;
        let height = image_height as f64 * scale;

        (width as i32, height as i32)
    }
}
