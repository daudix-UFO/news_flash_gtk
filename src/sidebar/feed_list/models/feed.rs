use diffus::{Diffable, Same};
use news_flash::models::{CategoryID, Feed, FeedMapping};
use std::cmp::Ordering;
use std::fmt::{self, Display};
use std::sync::Arc;

use super::FeedListItemID;

#[derive(Clone, Debug)]
pub struct FeedListFeedModel {
    pub id: FeedListItemID,
    pub parent_id: CategoryID,
    pub label: String,
    pub sort_index: i32,
    pub level: i32,
    pub item_count: i64,
}

impl FeedListFeedModel {
    pub fn new(feed: &Feed, mapping: &FeedMapping, item_count: i64, level: i32) -> Self {
        FeedListFeedModel {
            id: FeedListItemID::Feed(Arc::new(mapping.clone())),
            parent_id: mapping.category_id.clone(),
            label: feed.label.clone(),
            sort_index: match mapping.sort_index {
                Some(index) => index,
                None => std::i32::MAX,
            },
            level,
            item_count,
        }
    }
}

impl PartialEq for FeedListFeedModel {
    fn eq(&self, other: &FeedListFeedModel) -> bool {
        self.id == other.id //&& self.sort_index == other.sort_index
    }
}

impl Eq for FeedListFeedModel {}

impl Ord for FeedListFeedModel {
    fn cmp(&self, other: &FeedListFeedModel) -> Ordering {
        self.sort_index.cmp(&other.sort_index)
    }
}

impl PartialOrd for FeedListFeedModel {
    fn partial_cmp(&self, other: &FeedListFeedModel) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl fmt::Display for FeedListFeedModel {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} (count: {}) (id: {})", self.label, self.item_count, self.id)
    }
}

impl Same for FeedListFeedModel {
    fn same(&self, other: &Self) -> bool {
        self.id == other.id && self.parent_id == other.parent_id
    }
}

impl<'a> Diffable<'a> for FeedListFeedModel {
    type Diff = FeedDiff;

    fn diff(&'a self, other: &'a Self) -> diffus::edit::Edit<Self> {
        let label = if self.label == other.label {
            None
        } else {
            Some(other.label.clone())
        };

        let item_count = if self.item_count == other.item_count {
            None
        } else {
            Some(other.item_count)
        };

        if self == other && label.is_none() && item_count.is_none() {
            diffus::edit::Edit::Copy(self)
        } else {
            diffus::edit::Edit::Change(FeedDiff {
                id: self.id.clone(),
                label,
                item_count,
            })
        }
    }
}

pub struct FeedDiff {
    pub id: FeedListItemID,
    pub label: Option<String>,
    pub item_count: Option<i64>,
}

impl Display for FeedDiff {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?} (count: {:?}) (id: {})", self.label, self.item_count, self.id)
    }
}
