use crate::about_dialog::APP_NAME;
use crate::app::App;
use crate::config::{APP_ID, PROFILE};
use crate::content_page::ContentPage;
use crate::login_screen::{CustomApiSecret, PasswordLogin, WebLogin};
use crate::reset_page::ResetPage;
use crate::settings::Keybindings;
use crate::sidebar::models::SidebarSelection;
use crate::sidebar::FeedListItemID;
use crate::undo_action::UndoDelete;
use crate::util::constants;
use crate::welcome_screen::WelcomePage;
use gdk4::RGBA;
use gio::ListStore;
use glib::{self, clone, subclass, ControlFlow, Propagation};
use gtk4::{self, prelude::*, subclass::prelude::*, CallbackAction, CompositeTemplate, Shortcut, ShortcutTrigger};
use libadwaita::traits::NavigationPageExt;
use libadwaita::{subclass::prelude::*, NavigationView};
use log::error;
use news_flash::models::{ApiSecret, ArticleID, LoginData, LoginGUI, PluginID, PluginInfo};
use news_flash::{error::NewsFlashError, NewsFlash};
use std::cell::RefCell;
use std::rc::Rc;
use std::time::Duration;

const CONTENT_PAGE: &str = "content_page";
const WELCOME_PAGE: &str = "welcome_page";
const RESET_PAGE: &str = "reset_page";
const WEB_PAGE: &str = "web_login";
const PASSWORD_PAGE: &str = "password_login";
const API_SECRET_PAGE: &str = "api_secret_page";

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/main_window.blp")]
    pub struct MainWindow {
        #[template_child]
        pub navigation_view: TemplateChild<NavigationView>,
        #[template_child]
        pub welcome_page: TemplateChild<WelcomePage>,
        #[template_child]
        pub reset_page: TemplateChild<ResetPage>,
        #[template_child]
        pub content_page: TemplateChild<ContentPage>,
        #[template_child]
        pub password_login: TemplateChild<PasswordLogin>,
        #[template_child]
        pub web_login: TemplateChild<WebLogin>,
        #[template_child]
        pub api_secret_page: TemplateChild<CustomApiSecret>,

        #[template_child]
        pub shortcut_list: TemplateChild<ListStore>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for MainWindow {
        const NAME: &'static str = "MainWindow";
        type ParentType = libadwaita::ApplicationWindow;
        type Type = super::MainWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for MainWindow {}

    impl WidgetImpl for MainWindow {}

    impl WindowImpl for MainWindow {}

    impl ApplicationWindowImpl for MainWindow {}

    impl AdwApplicationWindowImpl for MainWindow {}
}

glib::wrapper! {
    pub struct MainWindow(ObjectSubclass<imp::MainWindow>)
        @extends gtk4::Widget, gtk4::Window, gtk4::ApplicationWindow, libadwaita::ApplicationWindow,
        @implements gio::ActionMap, gio::ActionGroup;
}

impl Default for MainWindow {
    fn default() -> Self {
        Self::new()
    }
}

impl MainWindow {
    pub fn new() -> Self {
        let window = glib::Object::new::<Self>();
        window.set_icon_name(Some(APP_ID));
        window.set_title(Some(APP_NAME));
        if PROFILE == "Devel" {
            window.add_css_class("devel");
        }
        window
    }

    pub fn init(&self, shutdown_in_progress: Rc<RefCell<bool>>) {
        let imp = self.imp();

        libadwaita::StyleManager::default().connect_dark_notify(
            clone!(@weak self as window => @default-panic, move |sm| {
                let is_dark = sm.is_dark();
                glib::timeout_add_local(Duration::from_millis(20), move || {
                        window.content_page().load_branding();
                        let rgba = if is_dark { RGBA::BLACK } else { RGBA::WHITE };
                        window.content_page().articleview_column().article_view().update_background_color(&rgba);
                        ControlFlow::Break
                    }
                );
            }),
        );

        // setup shutdown
        let content_page = self.content_page();
        self.connect_close_request(clone!(
            @weak content_page,
            @strong shutdown_in_progress,
            @weak self as window => @default-panic, move |win|
        {
            let imp = window.imp();

            if *shutdown_in_progress.borrow() {
                win.set_visible(false);
                return Propagation::Stop;
            }
            if App::default().settings().borrow().get_keep_running_in_background() {

                if let Some(visible_page) = imp.navigation_view.visible_page() {
                    if let Some(tag) = visible_page.tag() {
                        if tag.as_str() == CONTENT_PAGE {
                            win.set_visible(false);
                            return Propagation::Stop;
                        }
                    }
                }
            }

            App::default().queue_quit();
            Propagation::Stop
        }));

        self.connect_fullscreened_notify(move |window| {
            let window_fullscreen = window.is_fullscreened();
            window.content_page().outer().set_collapsed(window_fullscreen);
            window.content_page().inner().set_collapsed(window_fullscreen);
            window.content_page().inner().set_show_content(window_fullscreen);
        });

        self.setup_shortcuts();

        // setup background permissions
        if App::default().settings().borrow().get_keep_running_in_background() {
            App::request_background_permission(App::default().settings().borrow().get_autostart());
        }

        // set visible page
        imp.navigation_view.replace_with_tags(&[CONTENT_PAGE]);
        self.content_page().load_branding();
    }

    fn add_shortcut<P: Fn() + 'static>(&self, id: &str, func: P, allow_offline: bool) {
        if let Ok(Some(keybinding)) = Keybindings::read_keybinding(id) {
            let trigger = ShortcutTrigger::parse_string(&keybinding);
            let action = CallbackAction::new(clone!(@weak self as this => @default-panic, move |_widget, _variant| {
                // ignore shortcuts when not on content page
                if let Some(visible_page) = this.imp().navigation_view.visible_page() {
                    if let Some(tag) = visible_page.tag() {
                        if tag.as_str() != CONTENT_PAGE {
                            return false;
                        }
                    }
                }

                // ignore shortcuts when typing in search entry
                if this.content_page().article_list_column().is_search_focused() {
                    return false;
                }

                let is_online = !this.content_page().state().borrow().get_offline();

                if allow_offline || is_online {
                    func();
                }

                true
            }));
            let shortcut = Shortcut::new(trigger, Some(action));
            self.imp().shortcut_list.append(&shortcut);
        }
    }

    pub fn setup_shortcuts(&self) {
        self.imp().shortcut_list.remove_all();

        self.add_shortcut("shortcuts", || App::default().spawn_shortcut_window(), true);
        self.add_shortcut("refresh", || App::default().sync(), false);
        self.add_shortcut("quit", || App::default().queue_quit(), true);
        self.add_shortcut(
            "search",
            || {
                App::default()
                    .main_window()
                    .content_page()
                    .article_list_column()
                    .focus_search()
            },
            true,
        );
        self.add_shortcut(
            "all_articles",
            || {
                App::default()
                    .main_window()
                    .content_page()
                    .article_list_column()
                    .set_view_switcher_stack("all")
            },
            true,
        );
        self.add_shortcut(
            "only_unread",
            || {
                App::default()
                    .main_window()
                    .content_page()
                    .article_list_column()
                    .set_view_switcher_stack("unread")
            },
            true,
        );
        self.add_shortcut(
            "only_starred",
            || {
                App::default()
                    .main_window()
                    .content_page()
                    .article_list_column()
                    .set_view_switcher_stack("marked")
            },
            true,
        );
        self.add_shortcut(
            "next_article",
            || {
                App::default()
                    .main_window()
                    .content_page()
                    .article_list_column()
                    .article_list()
                    .select_next_article()
            },
            true,
        );
        self.add_shortcut(
            "previous_article",
            || {
                App::default()
                    .main_window()
                    .content_page()
                    .article_list_column()
                    .article_list()
                    .select_prev_article()
            },
            true,
        );
        self.add_shortcut(
            "toggle_category_expanded",
            || {
                App::default()
                    .main_window()
                    .content_page()
                    .sidebar_column()
                    .sidebar()
                    .expand_collapse_selected_category()
            },
            true,
        );
        self.add_shortcut("toggle_read", || App::default().toggle_selected_article_read(), true);
        self.add_shortcut(
            "toggle_marked",
            || App::default().toggle_selected_article_marked(),
            true,
        );
        self.add_shortcut(
            "open_browser",
            || App::default().open_selected_article_in_browser(),
            true,
        );
        self.add_shortcut(
            "copy_url",
            || App::default().copy_selected_article_url_to_clipboard(),
            true,
        );
        self.add_shortcut(
            "next_item",
            || App::default().main_window().content_page().sidebar_select_next_item(),
            true,
        );
        self.add_shortcut(
            "previous_item",
            || App::default().main_window().content_page().sidebar_select_prev_item(),
            true,
        );
        self.add_shortcut(
            "scroll_up",
            || {
                App::default()
                    .main_window()
                    .content_page()
                    .article_view_scroll_diff(-150.0)
            },
            true,
        );
        self.add_shortcut(
            "scroll_down",
            || {
                App::default()
                    .main_window()
                    .content_page()
                    .article_view_scroll_diff(150.0)
            },
            true,
        );
        self.add_shortcut("scrap_content", || App::default().grab_article_content(), true);
        self.add_shortcut(
            "tag",
            || {
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .popup_tag_popover()
            },
            true,
        );
        self.add_shortcut(
            "fullscreen",
            || {
                if let Some(action) = App::default().main_window().lookup_action("fullscreen-article") {
                    action.activate(None);
                }
            },
            true,
        );
        self.add_shortcut(
            "sidebar_set_read",
            || App::default().main_window().set_sidebar_read(),
            false,
        );
    }

    pub fn content_page(&self) -> &ContentPage {
        let imp = self.imp();
        &imp.content_page
    }

    pub fn show_undo_bar(&self, action: UndoDelete) {
        let select_all_button = match self.content_page().sidebar_column().sidebar().get_selection() {
            SidebarSelection::All => false,
            SidebarSelection::FeedList(selected_id, _label) => match &action {
                UndoDelete::Category(delete_id, _label) => selected_id == FeedListItemID::Category(delete_id.clone()),
                UndoDelete::Feed(delete_id, _label) => {
                    if let FeedListItemID::Feed(mapping) = &selected_id {
                        &mapping.feed_id == delete_id
                    } else {
                        false
                    }
                }
                _ => false,
            },

            SidebarSelection::Tag(selected_id, _label) => match &action {
                UndoDelete::Tag(delete_id, _label) => &selected_id == delete_id,
                _ => false,
            },
        };
        if select_all_button {
            self.content_page()
                .state()
                .borrow_mut()
                .set_sidebar_selection(SidebarSelection::All);
            self.content_page()
                .sidebar_column()
                .sidebar()
                .select_all_button_no_update();
        }

        self.content_page().add_undo_notification(action);
    }

    pub fn show_welcome_page(&self) {
        let imp = self.imp();

        imp.navigation_view.replace_with_tags(&[WELCOME_PAGE]);
    }

    pub fn show_login_page(&self, plugin_id: &PluginID, data: Option<LoginData>) {
        let imp = self.imp();

        imp.web_login.reset();
        imp.password_login.reset();
        imp.api_secret_page.reset();

        if let Some(info) = NewsFlash::list_backends().get(plugin_id) {
            match &info.login_gui {
                LoginGUI::OAuth(oauth_info) => {
                    if oauth_info.custom_api_secret {
                        imp.api_secret_page.set_service(info);

                        if let Some(LoginData::OAuth(oauth_data)) = data {
                            if let Some(api_secret) = &oauth_data.custom_api_secret {
                                imp.api_secret_page.fill(api_secret);
                            } else {
                                imp.api_secret_page.reset();
                            }
                        }

                        imp.navigation_view.push_by_tag(API_SECRET_PAGE);
                    } else if let Ok(()) = imp.web_login.set_service(info) {
                        imp.navigation_view.push_by_tag(WEB_PAGE);
                    }
                }
                LoginGUI::Direct(_) => {
                    if let Ok(()) = imp.password_login.set_service(info) {
                        if let Some(LoginData::Direct(data)) = data {
                            imp.password_login.fill(data);
                        } else {
                            imp.password_login.reset();
                        }
                        imp.navigation_view.push_by_tag(PASSWORD_PAGE);
                    }
                }
                LoginGUI::None => {}
            }
        }
    }

    pub fn show_web_login_page(&self, info: &PluginInfo, secret: Option<&ApiSecret>) {
        let imp = self.imp();

        _ = imp.web_login.set_service(info);
        imp.web_login.set_custom_api_secret(secret);
        imp.navigation_view.push_by_tag(WEB_PAGE);
    }

    pub fn show_login_error(&self, error: NewsFlashError, data: &LoginData) {
        let imp = self.imp();

        match &data {
            LoginData::OAuth(_) => imp.web_login.show_error(error),
            LoginData::Direct(_) => imp.password_login.show_error(error),
            LoginData::None(_) => {}
        }
    }

    pub fn show_content_page(&self) {
        let imp = self.imp();

        imp.navigation_view.replace_with_tags(&[CONTENT_PAGE]);

        App::default().update_sidebar();

        // show discover dialog after login with local rss
        App::default().execute_with_callback(
            |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash.id().await
                } else {
                    None
                }
            },
            |app, res| {
                if res.map(|id| id.as_str() == "local_rss").unwrap_or(false) {
                    app.spawn_discover_dialog();
                }
            },
        );

        self.content_page().load_branding();
    }

    pub fn show_reset_page(&self) {
        let imp = self.imp();
        imp.reset_page.reset();
        imp.navigation_view.push_by_tag(RESET_PAGE);
    }

    pub fn reset_account_failed(&self, error: NewsFlashError) {
        let imp = self.imp();
        imp.reset_page.error(error);
    }

    pub fn sidebar_selection(&self, selection: SidebarSelection) {
        self.content_page().responsive_layout().show_sidebar();

        if &selection != self.content_page().state().borrow_mut().get_sidebar_selection() {
            self.content_page()
                .state()
                .borrow_mut()
                .set_sidebar_selection(selection);
            App::default().update_article_list();
        }
    }

    pub fn show_article(&self, article_id: ArticleID) {
        self.content_page()
            .state()
            .borrow_mut()
            .set_prefer_scraped_content(true);

        App::default().execute_with_callback(
            move |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    if let Ok(fat_article) = news_flash.get_fat_article(&article_id) {
                        let enclosures = news_flash.get_enclosures(&article_id).ok().and_then(|enclosures| {
                            if enclosures.is_empty() {
                                None
                            } else {
                                Some(enclosures)
                            }
                        });
                        let feed_title = news_flash
                            .get_feeds()
                            .ok()
                            .and_then(|(feeds, _)| feeds.into_iter().find(|f| f.feed_id == fat_article.feed_id))
                            .map(|f| f.label)
                            .unwrap_or(constants::UNKNOWN_FEED.into());
                        Some((fat_article, feed_title, enclosures))
                    } else {
                        None
                    }
                } else {
                    None
                }
            },
            |app, res| {
                if let Some((fat_article, feed_title, enclosures)) = res {
                    let main_window = app.main_window();
                    let content_page = main_window.content_page();
                    let article_view_column = content_page.articleview_column();

                    content_page
                        .state()
                        .borrow_mut()
                        .set_visible_article_id(Some(&fat_article.article_id));
                    article_view_column.show_article(Some(&fat_article), enclosures.as_ref());
                    article_view_column
                        .article_view()
                        .show_article(fat_article, feed_title, enclosures);

                    let show_article_view = content_page
                        .state()
                        .borrow_mut()
                        .get_article_view_visible()
                        .unwrap_or(true);
                    content_page.responsive_layout().show_article_view(show_article_view);
                }
            },
        );
    }

    pub fn set_search_term(&self, search_term: String) {
        let empty_search = search_term.is_empty();

        let update_article_list =
            if let Some(current_search_term) = self.content_page().state().borrow().get_search_term() {
                current_search_term != search_term
            } else {
                !empty_search
            };

        if empty_search {
            self.content_page().state().borrow_mut().set_search_term(None);
        } else {
            self.content_page()
                .state()
                .borrow_mut()
                .set_search_term(Some(search_term));
        }

        if update_article_list {
            App::default().update_article_list();
        }
    }

    pub fn set_sidebar_read(&self) {
        match self.content_page().state().borrow().get_sidebar_selection() {
            SidebarSelection::All => {
                App::default().execute_with_callback(
                    |news_flash, client| async move {
                        if let Some(news_flash) = news_flash.read().await.as_ref() {
                            news_flash.set_all_read(&client).await
                        } else {
                            Err(NewsFlashError::NotLoggedIn)
                        }
                    },
                    |app, res| {
                        if let Err(error) = res {
                            let message = "Failed to mark all read".to_owned();
                            error!("{message}");
                            App::default().in_app_error(&message, error);
                        }

                        app.main_window()
                            .content_page()
                            .article_list_column()
                            .finish_mark_all_read();
                        App::default().update_article_header();
                        App::default().update_article_list();
                        App::default().update_sidebar();
                    },
                );
            }
            SidebarSelection::FeedList(item_id, _title) => match &item_id {
                FeedListItemID::Feed(feed_mapping) => {
                    let feed_id_vec = vec![feed_mapping.feed_id.clone()];
                    App::default().execute_with_callback(
                        |news_flash, client| async move {
                            if let Some(news_flash) = news_flash.read().await.as_ref() {
                                news_flash.set_feed_read(&feed_id_vec, &client).await
                            } else {
                                Err(NewsFlashError::NotLoggedIn)
                            }
                        },
                        |app, res| {
                            if let Err(error) = res {
                                let message = "Failed to mark all read".to_owned();
                                error!("{message}");
                                App::default().in_app_error(&message, error);
                            }

                            app.main_window()
                                .content_page()
                                .article_list_column()
                                .finish_mark_all_read();
                            App::default().update_article_header();
                            App::default().update_article_list();
                            App::default().update_sidebar();
                        },
                    );
                }
                FeedListItemID::Category(category_id) => {
                    let category_id_vec = vec![category_id.clone()];
                    App::default().execute_with_callback(
                        |news_flash, client| async move {
                            if let Some(news_flash) = news_flash.read().await.as_ref() {
                                news_flash.set_category_read(&category_id_vec, &client).await
                            } else {
                                Err(NewsFlashError::NotLoggedIn)
                            }
                        },
                        |app, res| {
                            if let Err(error) = res {
                                let message = "Failed to mark all read".to_owned();
                                error!("{message}");
                                App::default().in_app_error(&message, error);
                            }

                            app.main_window()
                                .content_page()
                                .article_list_column()
                                .finish_mark_all_read();
                            App::default().update_article_header();
                            App::default().update_article_list();
                            App::default().update_sidebar();
                        },
                    );
                }
            },
            SidebarSelection::Tag(tag_id, _title) => {
                let tag_id_vec = vec![tag_id.clone()];
                App::default().execute_with_callback(
                    |news_flash, client| async move {
                        if let Some(news_flash) = news_flash.read().await.as_ref() {
                            news_flash.set_tag_read(&tag_id_vec, &client).await
                        } else {
                            Err(NewsFlashError::NotLoggedIn)
                        }
                    },
                    |app, res| {
                        if let Err(error) = res {
                            let message = "Failed to mark all read".to_owned();
                            error!("{message}");
                            App::default().in_app_error(&message, error);
                        }

                        app.main_window()
                            .content_page()
                            .article_list_column()
                            .finish_mark_all_read();
                        App::default().update_article_header();
                        App::default().update_article_list();
                        App::default().update_sidebar();
                    },
                );
            }
        }
    }

    pub fn update_article_header(&self) {
        let (visible_article, visible_article_enclosures) = self
            .content_page()
            .articleview_column()
            .article_view()
            .get_visible_article();

        let visible_article = if let Some(visible_article) = visible_article {
            visible_article
        } else {
            return;
        };

        App::default().execute_with_callback(
            |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash
                        .get_fat_article(&visible_article.article_id)
                        .unwrap_or(visible_article)
                } else {
                    visible_article
                }
            },
            move |app, updated_visible_article| {
                app.main_window()
                    .content_page()
                    .articleview_column()
                    .show_article(Some(&updated_visible_article), visible_article_enclosures.as_ref());
                app.main_window()
                    .content_page()
                    .articleview_column()
                    .article_view()
                    .update_visible_article(Some(updated_visible_article.unread), None);
            },
        );
    }

    pub fn update_features(&self) {
        self.content_page().sidebar_column().update_features();
    }
}
